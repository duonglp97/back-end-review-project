package com.example.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@ComponentScan(basePackages={"com.example.database.service","com.example.database.extension", "com.example.admin.config", "com.example.admin.controller", "com.example.admin.apiController"})
@EnableJpaRepositories(basePackages={"com.example.database.repository"})
@EntityScan(basePackages={"com.example.database.entity"})
@SpringBootApplication
public class AdminApplication {

    public static void main(String[] args) {
        SpringApplication.run(AdminApplication.class, args);
    }

}
