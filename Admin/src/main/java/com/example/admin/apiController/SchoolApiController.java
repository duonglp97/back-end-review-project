package com.example.admin.apiController;

import com.example.database.entity.School;
import com.example.database.model.BaseApiResult;
import com.example.database.model.ChartDTO;
import com.example.database.model.DataApiResult;
import com.example.database.service.IReviewService;
import com.example.database.service.ISchoolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;


@RestController
public class SchoolApiController {
    @Autowired
    private ISchoolService iSchoolService;
    @Autowired
    private IReviewService iReviewService;
    @GetMapping("/chart")
    public BaseApiResult getChart(){
        DataApiResult result = new DataApiResult();
        ChartDTO dto = new ChartDTO();
        List<String> key1 = new ArrayList<>();
        List<String> value = new ArrayList<>();
        try{
            List<School> schoolList = iSchoolService.findAll();
            for (School school : schoolList){
                long countReview = iReviewService.countBySchool(school);
                Map<String, Long> mapReviewBySchool = new HashMap<String,Long>();
                mapReviewBySchool.put(school.getName(),countReview);
                Set<String> set = mapReviewBySchool.keySet();
                for (String key : set){
                    for (int i = 1; i <= set.size(); i++){
                        key1.add(key);
                        value.add(mapReviewBySchool.get(key).toString());
                    }

                }
            }
            dto.setKey(key1);
            dto.setValue(value);
            result.setData(dto);
            result.setSuccess(true);
            result.setMessage("");
        } catch (Exception e) {
            result.setSuccess(false);
            result.setMessage(e.getMessage());
        }
        return result;
    }
}
