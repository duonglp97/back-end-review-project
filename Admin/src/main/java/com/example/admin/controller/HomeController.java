package com.example.admin.controller;
//
//import com.example.database.service.IProductService;
import com.example.database.entity.*;
import com.example.database.extension.FileStorageService;
import com.example.database.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Controller
public class HomeController {




    @Autowired
    private FileStorageService fileStorageService;
    @Autowired
    private IImageService imageService;
    @Autowired
    private ISchoolService iSchoolService;
    @Autowired
    private IReviewService iReviewService;
//
//    @Autowired
//    private IProductService productService;
//
//    @RequestMapping(value = {"/admin"}, method = RequestMethod.GET)
//    public String index(Model model) {
//
//        model.addAttribute("name", "Home index");
//        model.addAttribute("total_product", productService.getTotalProduct());
//
//        return "index";
//    }
    @GetMapping(value = {"/index","/"})
    public String index(Model model){
        List<School> schoolList = iSchoolService.findAll();
        for (School school : schoolList){
            long countReview = iReviewService.countBySchool(school);
            Map<String, Long> mapReviewBySchool = new HashMap<String,Long>();
            mapReviewBySchool.put(school.getName(),countReview);
            Set<String> set = mapReviewBySchool.keySet();
            for (String key : set){
                for (int i = 1; i <= set.size(); i++){
                    model.addAttribute("key " + i,key);
                    model.addAttribute("value " + i, mapReviewBySchool.get(key));
                }

            }
        }
        return "index";
    }


    @GetMapping(value = "/login")
    public String login(Model model){
        return "login";
    }




}