package com.example.admin.controller;

import com.example.database.entity.Contact;
import com.example.database.service.IContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Controller
public class ContactController {
    @Autowired
    private IContactService iContactService;

    @GetMapping(value = "/manage-contact")
    public String contact(Model model){
        List<Contact> contactList = iContactService.findAll();
        model.addAttribute("contactList" , contactList);
        return "manageContact";
    }

    @GetMapping(value = "/manage-contact/delete/{id}")
    public String deleteContact(@PathVariable("id") int id, RedirectAttributes redirectAttributes){
        iContactService.deleteById(id);
        redirectAttributes.addFlashAttribute("success","Bạn đã xóa liên hệ thành công!");
        return "redirect:/manage-contact";
    }
}
