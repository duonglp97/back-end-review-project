package com.example.admin.controller;

import com.example.database.constant.ReviewConstant;
import com.example.database.entity.Image;
import com.example.database.entity.Review;
import com.example.database.entity.School;
import com.example.database.service.IPersonService;
import com.example.database.service.IReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.security.Principal;
import java.util.List;

@Controller
public class ReviewController {
    @Autowired
    private IReviewService reviewService;
    @Autowired
    @Qualifier(value = "personServiceAdminImpl")
    private IPersonService personService;

    @GetMapping(value = "/manage-review")
    public String manageReview(Model model){
        List<Review> reviewList = reviewService.findAll();
        model.addAttribute("reviewList",reviewList);
        Review review = new Review();
        model.addAttribute("review",review);
        return "manageReview";
    }

    public String saveReview(@ModelAttribute Review review, BindingResult result, RedirectAttributes redirect){
        if (result.hasErrors()){
            return "manageReview";
        }
        try {


            if (reviewService.findOne(review.getId())!=null){
                Review updateReivew = reviewService.findOne(review.getId());
                updateReivew.setTitle(review.getTitle());
                updateReivew.setPublish(review.getPublish());
                updateReivew.setAnonymous(review.getAnonymous());
                updateReivew.setWeakness(review.getWeakness());
                updateReivew.setAdvantage(review.getAdvantage());
                updateReivew.setSchool(review.getSchool());
//                con thieu get person
                reviewService.save(updateReivew);

            }else {


                Review reviewEntity = new Review();
                reviewEntity.setTitle(review.getTitle());
                reviewEntity.setPublish(review.getPublish());
                reviewEntity.setAnonymous(review.getAnonymous());
                reviewEntity.setWeakness(review.getWeakness());
                reviewEntity.setAdvantage(review.getAdvantage());
                reviewEntity.setSchool(review.getSchool());
                reviewService.save(reviewEntity);



            }


        }catch (Exception e){
            e.printStackTrace();
        }
        redirect.addFlashAttribute("success","Bạn đã tạo trung tâm thành công!");
        return "redirect:/manage-review";
    }

    @GetMapping(value = "/manage-review/delete/{id}")
    public String deleteReview(@PathVariable("id") int id, RedirectAttributes redirectAttributes){
        Review deleteReview = reviewService.findOne(id);
        reviewService.delete(id);

        redirectAttributes.addFlashAttribute("success","Bạn đã xóa bài reivew thành công!");
        return "redirect:/manage-review";


    }
    @GetMapping(value = "/manage-review/publishReview/{id}")
    public String publishReivew(@PathVariable("id") int id, RedirectAttributes redirectAttributes){
        Review review = reviewService.findOne(id);
        if (review.getPublish()== ReviewConstant.UnAnonymous){
            review.setPublish(ReviewConstant.Publish);
            reviewService.save(review);
            redirectAttributes.addFlashAttribute("success","Bạn đã đăng bài reivew thành công!");
        }else {
            review.setPublish(ReviewConstant.UnPublish);
            reviewService.save(review);
            redirectAttributes.addFlashAttribute("success","Bạn đã gỡ bài reivew thành công!");
        }

        return "redirect:/manage-review";
    }

}
