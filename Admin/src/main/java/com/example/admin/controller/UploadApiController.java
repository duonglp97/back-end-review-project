package com.example.admin.controller;

import com.example.database.extension.FileStorageService;
import com.example.database.model.FileUploadResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/api")
public class UploadApiController {

    @Autowired
    FileStorageService storageService;

    @PostMapping("/upload-image")
    public FileUploadResult uploadImage(
            @RequestParam("files") MultipartFile[] files) {
        String message = "";
        String link = "";
        FileUploadResult result = new FileUploadResult();
        try {
            for(MultipartFile file : files){
                String newFilename = storageService.store(file);
                message = "You successfully uploaded " +
                        file.getOriginalFilename() + "!";
                link += newFilename + ";";
            }
            result.setMessage(message);
            result.setSuccess(true);
            result.setLink(link);
        } catch (Exception e) {
            result.setSuccess(false);
            result.setMessage(e.getMessage());
        }
        return result;
    }

}