package com.example.admin.controller;


import com.example.database.entity.Center;
import com.example.database.entity.Image;
import com.example.database.entity.School;
import com.example.database.extension.FileStorageService;
import com.example.database.model.CenterDTO;
import com.example.database.model.DataApiResult;
import com.example.database.model.SchoolDTO;
import com.example.database.service.ICenterService;
import com.example.database.service.IImageService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Controller
public class CenterController {
    @Autowired
    private ICenterService centerService;
    @Autowired
    private FileStorageService fileStorageService;
    @Autowired
    private IImageService imageService;

    @Autowired
    ModelMapper modelMapper;
    @GetMapping(value = "/manage-center")
    public String manageCenter(Model model){
        List<Center> centerList = centerService.findAll();
        model.addAttribute("centerList",centerList);

        Center center = new Center();
        model.addAttribute("center",center);
        return "manageCenter";
    }

    @PostMapping(value = "/manage-center/save")
    public String saveCenter(@RequestParam("image") List<MultipartFile> img, @ModelAttribute Center center, BindingResult result, RedirectAttributes redirect){
        if (result.hasErrors()){
            return "manageCenter";
        }
        try {




            if (centerService.findById(center.getId())!=null){
                Center updateCenter = centerService.findById(center.getId());
                updateCenter.setName(center.getName());
                updateCenter.setAddress(center.getAddress());
                updateCenter.setSale(center.getSale());

                centerService.save(updateCenter);
//                if (imageService.findByUrl(fileName)!=null) {
//                    Image updateImage = imageService.findByUrl(fileName);
//                    updateImage.setUrl(fileName);
//                    updateImage.setName(center.getName());
//                    updateImage.setCenter(updateCenter);
//                    imageService.save(updateImage);
//                }

                for (MultipartFile file : img){
                    String fileName = fileStorageService.store(file);
                    Image imageEntity = new Image();
                    imageEntity.setUrl(fileName);
                    imageEntity.setName(center.getName());
                    imageEntity.setCenter(updateCenter);
                    imageService.save(imageEntity);
                }

                redirect.addFlashAttribute("success","Bạn đã cập nhật trung tâm thành công!");
            }else {


                Center centerEntity = new Center();
                centerEntity.setName(center.getName());
                centerEntity.setAddress(center.getAddress());
                centerEntity.setSale(center.getSale());
                centerService.save(centerEntity);
                String[] lstFile = center.getLstFile().split(";");
                for (int i = 0; i< lstFile.length; i++){
                    String fileName = lstFile[i];
                    Image imageEntity = new Image();
                    imageEntity.setUrl(fileName);
                    imageEntity.setName(center.getName());
                    imageEntity.setCenter(centerEntity);
                    imageService.save(imageEntity);
                }





                redirect.addFlashAttribute("success","Bạn đã tạo trung tâm thành công!");

            }


        }catch (Exception e){
            e.printStackTrace();
        }

        return "redirect:/manage-center";
    }

    @GetMapping(value = "/manage-center/delete/{id}")
    public String deleteCenter(@PathVariable("id") int id, RedirectAttributes redirect){
        imageService.deleteAllByCenter(centerService.findById(id));
        centerService.delete(id);
        redirect.addFlashAttribute("success","Bạn đã xóa trung tâm thành công!");
        return "redirect:/manage-center";

    }

    @GetMapping(value = "/manage-center/prepareUpdate/{id}")
    public ResponseEntity updateCenter(@PathVariable("id") int id){
        DataApiResult dataApiResult = new DataApiResult();
        Center center = centerService.findById(id);
        CenterDTO dto = modelMapper.map(center,CenterDTO.class);
        dataApiResult.setSuccess(true);
        dataApiResult.setData(dto);
        return ResponseEntity.ok(dataApiResult);
    }

}
