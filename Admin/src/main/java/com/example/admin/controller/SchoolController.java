package com.example.admin.controller;

import com.example.database.entity.Image;
import com.example.database.entity.School;
import com.example.database.extension.FileStorageService;
import com.example.database.model.BaseApiResult;
import com.example.database.model.DataApiResult;
import com.example.database.model.ImageDTO;
import com.example.database.model.SchoolDTO;
import com.example.database.service.IImageService;
import com.example.database.service.IReviewService;
import com.example.database.service.ISchoolService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
public class SchoolController {
    @Autowired
    private ISchoolService schoolService;
    @Autowired
    private IImageService imageService;
    @Autowired
    private IReviewService reviewService;
    @Autowired
    private FileStorageService fileStorageService;
    @Autowired
    ModelMapper modelMapper;

    @GetMapping(value = "/manage-school")
    public String manageSchool(Model model){
        List<School> schoolList = schoolService.findAll();
        model.addAttribute("schoolList",schoolList);

        School school = new School();
        model.addAttribute("school",school);
        return "manageSchool";
    }
    @GetMapping(value = "/manage-school/delete/{id}")
    public String deleteSchool(@PathVariable("id") int id, RedirectAttributes redirectAttributes){
        School deleteSchool = schoolService.findById(id);
        imageService.deleteAllBySchool(deleteSchool);
        reviewService.deleteAllBySchool(deleteSchool);
        schoolService.deleteById(id);

        redirectAttributes.addFlashAttribute("success","Bạn đã xóa trường học thành công!");
        return "redirect:/manage-school";


    }

    @GetMapping(value = "/manage-school/prepareUpdate/{id}")
    public ResponseEntity updateSchool(@PathVariable("id") int id){
        DataApiResult dataApiResult = new DataApiResult();
        School school = schoolService.findById(id);
        SchoolDTO dto = modelMapper.map(school, SchoolDTO.class);
        List<Image> images = imageService.findImagesBySchool(school.getId());
        List<ImageDTO> imageDTOS = new ArrayList<>();
        for(Image i : images){
            imageDTOS.add(modelMapper.map(i, ImageDTO.class));
        }
        dto.setImages(imageDTOS);
        dataApiResult.setSuccess(true);
        dataApiResult.setData(dto);
        return ResponseEntity.ok(dataApiResult);
    }

    @PostMapping(value = "/manage-school/save")
    public String saveSchool(@RequestParam("image") List<MultipartFile> img, @Valid School school, BindingResult result, RedirectAttributes redirect){
        if (result.hasErrors()){
            return "manageSchool";
        }
        try {


            if (schoolService.findById(school.getId())!=null){
                School updateSchool = schoolService.findById(school.getId());
                updateSchool.setName(school.getName());
                updateSchool.setAddress(school.getAddress());
                updateSchool.setHistory(school.getHistory());
                updateSchool.setMajor("Công nghệ thông tin");
                schoolService.save(updateSchool);
//                if (imageService.findByUrl(fileName)!=null) {
//                    Image updateImage = imageService.findByUrl(fileName);
//                    updateImage.setUrl(fileName);
//                    updateImage.setName(school.getName());
//                    updateImage.setSchool(updateSchool);
//                    imageService.save(updateImage);
//
//                }
//                Image imageEntity = new Image();
//                String fileName = fileStorageService.store(img);
//                imageEntity.setUrl(fileName);
//                imageEntity.setName(school.getName());
//                imageEntity.setSchool(updateSchool);
//                imageService.save(imageEntity);
                redirect.addFlashAttribute("success","Bạn đã cập nhật trung tâm thành công!");
            }else {


                School schoolEntity = new School();
                schoolEntity.setName(school.getName());
                schoolEntity.setAddress(school.getAddress());
                schoolEntity.setHistory(school.getHistory());
                schoolEntity.setMajor("Công nghệ thông tin");
                schoolService.save(schoolEntity);
                String[] lstFile = school.getLstFile().split(";");
                for (int i = 0; i< lstFile.length; i++){
                    String fileName = lstFile[i];
                    Image imageEntity = new Image();
                    imageEntity.setUrl(fileName);
                    imageEntity.setName(school.getName());
                    imageEntity.setSchool(schoolEntity);
                    imageService.save(imageEntity);
                }


                redirect.addFlashAttribute("success","Bạn đã tạo trung tâm thành công!");

            }


        }catch (Exception e){
            e.printStackTrace();
        }

        return "redirect:/manage-school";
    }


}
