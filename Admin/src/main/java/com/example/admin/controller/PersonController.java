package com.example.admin.controller;

import com.example.database.constant.PersonConstant;
import com.example.database.entity.Person;
import com.example.database.entity.Role;
import com.example.database.service.IPersonService;
import com.example.database.service.IRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
@Controller

public class PersonController {

    @Autowired
    @Qualifier("personServiceAdminImpl")
    private IPersonService personService;
    @Autowired
    private IRoleService roleService;

    @GetMapping(value = "/manage-person")
    public String managePerson(Model model){
        Role role = roleService.findByName("ROLE_MEMBER");
        List<Person> personList = personService.findAllByRoles(role);
        model.addAttribute("personList",personList);
        return "manageUser";
    }

    @GetMapping(value = "/manage-person/active/{id}")
    public String activePerson(@PathVariable("id") int id, RedirectAttributes redirectAttributes){
        Person person = personService.findById(id);
        if (person.getStatus()== PersonConstant.NONACTIVE_STATUS){
            person.setStatus(PersonConstant.ACTIVE_STATUS);
            personService.save(person);
            redirectAttributes.addFlashAttribute("success","Bạn đã kích hoạt người dùng thành công!");
        }else {
            person.setStatus(PersonConstant.NONACTIVE_STATUS);
            personService.save(person);
            redirectAttributes.addFlashAttribute("success","Bạn đã chặn người dùng thành công!");
        }
        return "redirect:/manage-person";
    }
}
