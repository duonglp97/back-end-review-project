package com.example.admin.config;

import com.example.database.entity.Person;
import com.example.database.entity.Review;
import com.example.database.entity.Role;
import com.example.database.entity.School;
import com.example.database.repository.PersonRepository;
import com.example.database.repository.ReviewRepository;
import com.example.database.repository.RoleRepository;
import com.example.database.repository.SchoolRepository;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

@Component
public class DataSeedingListener implements ApplicationListener<ContextRefreshedEvent> {
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private PersonRepository personRepository;
    @Autowired
    private SchoolRepository schoolRepository;
    @Autowired
    private ReviewRepository reviewRepository;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent arg0) {
//        if(roleRepository.findByName("ROLE_ADMIN")== null) {
//            roleRepository.save(new Role("ROLE_ADMIN"));
//        }
//
//        if(roleRepository.findByName("ROLE_MEMBER")== null) {
//            roleRepository.save(new Role("ROLE_MEMBER"));
//        }
//
//        if(personRepository.findByUserName("duong")== null) {
//            Person admin = new Person();
//            admin.setName("duong");
//            admin.setDob(new Date());
//            admin.setAddress("ha noi");
//            admin.setEmail("duonglp97@gmail.com");
//            admin.setPassword(passwordEncoder.encode("123456"));
//            admin.setUserName("duong");
//
//            HashSet<Role> roles = new HashSet<Role>();
//            roles.add(roleRepository.findByName("ROLE_ADMIN"));
//            admin.setRoles(roles);
//            personRepository.save(admin);
//
//        }
//        if(personRepository.findByUserName("huy")== null) {
//            Person member = new Person();
//            member.setUserName("huy");
//            member.setPassword(passwordEncoder.encode("123456"));
//            member.setName("huy");
//            HashSet<Role> roles = new HashSet<Role>();
//
//            roles.add(roleRepository.findByName("ROLE_MEMBER"));
//            member.setRoles(roles);
//            personRepository.save(member);
//
//        }
//        Gson gson = new Gson();
//
//        try (Reader reader = new FileReader("D:\\sql\\daihoccongnghiep.json")) {
//
//            // Convert JSON File to Java Object
//            Reviews staff = gson.fromJson(reader, Reviews.class);
//
//            // print staff object
////            System.out.println(staff.toString());
////            System.out.println(staff.getText1());
//            School school = new School();
//            school.setName("Trường đại học Công nghiệp Hà Nội");
//            school.setAddress(staff.getListLocation().get(1));
//            school.setMajor("Công nghệ thông tin");
//            school.setHistory("Trường đại học Công nghiệp Hà Nội được thành lập từ năm 1898. Trường chính thức được mang tên như hiện nay vào năm 2005. Trong quá trình hình thành và phát triển của mình, đại học Công nghiệp Hà Nội đã vinh dự 4 lần đón tiếp chủ tịch Hồ Chí Minh đến thăm trường.");
//            schoolRepository.save(school);
//
//            List<com.example.admin.config.Review> reviewList = staff.getListReview();
//            for (com.example.admin.config.Review review1 : reviewList){
//                Review review = new Review();
//                Person person = new Person();
//                Date dob = new Date();
//                dob.setYear(97);
//                dob.setMonth(5);
//                dob.setDate(4);
//                Random generator = new Random();
//                if (personRepository.findByUserName(review1.getName())==null) {
//                    person.setDob(dob);
//                    person.setEmail("qabeo@gmail.com");
//                    person.setAddress("Hoàng Đạo Thúy");
//                    person.setName(review1.getName());
//                    person.setUserName(review1.getName());
//                    person.setPassword(passwordEncoder.encode("123456"));
//                    HashSet<Role> roles = new HashSet<Role>();
//                    roles.add(roleRepository.findByName("ROLE_MEMBER"));
//                    person.setRoles(roles);
//                    personRepository.save(person);
//
//                    review.setSchool(school);
//                    review.setAdvantage(review1.getAdvantages());
//                    review.setEvaluation(generator.nextInt(4)+1);
//                    review.setTitle(review1.getRateText());
//                    review.setWeakness(review1.getPointsNeedImprovement());
//                    review.setPerson(person);
//                    review.setAnonymous(0);
//                    review.setPublish(1);
//                    reviewRepository.save(review);
//
//                }else {
//                    person = personRepository.findByUserName("Tien Anh");
//                    personRepository.save(person);
//
//                    review.setSchool(school);
//                    review.setAdvantage(review1.getAdvantages());
//                    review.setEvaluation(generator.nextInt(4)+1);
//                    review.setTitle(review1.getRateText());
//                    review.setWeakness(review1.getPointsNeedImprovement());
//                    review.setPerson(person);
//                    review.setAnonymous(0);
//                    review.setPublish(1);
//                    reviewRepository.save(review);
//                }
//
//
//
//            }
//
//
//
//
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

    }
    }

