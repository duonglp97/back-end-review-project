package com.example.admin;

import java.util.List;

public class Reviews {
    private String text1;
    private String text2;
    private String text3;
    private List<String> listLocation;
    private List<Review> listReview;

    public String getText1() {
        return text1;
    }

    public void setText1(String text1) {
        this.text1 = text1;
    }

    public String getText2() {
        return text2;
    }

    public void setText2(String text2) {
        this.text2 = text2;
    }

    public String getText3() {
        return text3;
    }

    public void setText3(String text3) {
        this.text3 = text3;
    }

    public List<String> getListLocation() {
        return listLocation;
    }

    public void setListLocation(List<String> listLocation) {
        this.listLocation = listLocation;
    }

    public List<Review> getListReview() {
        return listReview;
    }

    public void setListReview(List<Review> listReview) {
        this.listReview = listReview;
    }

    @Override
    public String toString() {
        return "Reviews{" +
                "text1='" + text1 + '\'' +
                ", text2='" + text2 + '\'' +
                ", text3='" + text3 + '\'' +
                ", listLocation=" + listLocation +
                ", listReview=" + listReview +
                '}';
    }
}
