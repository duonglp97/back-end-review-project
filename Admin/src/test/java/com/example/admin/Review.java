package com.example.admin;

public class Review {
    private String img;
    private String name;
    private String graduating;
    private String rateText;
    private String rates;
    private String learn;
    private String advantages;
    private String pointsNeedImprovement;
    private String experiencesAndTips;

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGraduating() {
        return graduating;
    }

    public void setGraduating(String graduating) {
        this.graduating = graduating;
    }

    public String getRateText() {
        return rateText;
    }

    public void setRateText(String rateText) {
        this.rateText = rateText;
    }

    public String getRates() {
        return rates;
    }

    public void setRates(String rates) {
        this.rates = rates;
    }

    public String getLearn() {
        return learn;
    }

    public void setLearn(String learn) {
        this.learn = learn;
    }

    public String getAdvantages() {
        return advantages;
    }

    public void setAdvantages(String advantages) {
        this.advantages = advantages;
    }

    public String getPointsNeedImprovement() {
        return pointsNeedImprovement;
    }

    public void setPointsNeedImprovement(String pointsNeedImprovement) {
        this.pointsNeedImprovement = pointsNeedImprovement;
    }

    public String getExperiencesAndTips() {
        return experiencesAndTips;
    }

    public void setExperiencesAndTips(String experiencesAndTips) {
        this.experiencesAndTips = experiencesAndTips;
    }

    @Override
    public String toString() {
        return "Review{" +
                "img='" + img + '\'' +
                ", name='" + name + '\'' +
                ", graduating='" + graduating + '\'' +
                ", rateText='" + rateText + '\'' +
                ", rates='" + rates + '\'' +
                ", learn='" + learn + '\'' +
                ", advantages='" + advantages + '\'' +
                ", pointsNeedImprovement='" + pointsNeedImprovement + '\'' +
                ", experiencesAndTips='" + experiencesAndTips + '\'' +
                '}';
    }
}
