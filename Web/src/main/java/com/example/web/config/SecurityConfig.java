package com.example.web.config;

import com.example.database.entity.Center;
import com.example.database.entity.Person;
import com.example.database.entity.Role;
import com.example.database.service.impl.PersonServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.thymeleaf.extras.springsecurity5.dialect.SpringSecurityDialect;

@Configuration
@EnableWebSecurity
@ComponentScan(basePackages={"com.example.database.service", "com.example.web.config", "com.example.web.controller"})
public class SecurityConfig extends WebSecurityConfigurerAdapter {


    @Qualifier("personServiceImpl")
    @Autowired
    private UserDetailsService userDetailsService;

    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Bean
    public SpringSecurityDialect springSecurityDialect(){
        return new SpringSecurityDialect();
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception{
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
    }

    @Override
    protected void configure(HttpSecurity security) throws Exception
    {
        security.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.ALWAYS).and()

                .authorizeRequests()

                    .antMatchers("/register","/","/school/search","/school/{id}","/center/{id}","/contact","/contact/save","/login").permitAll()
                    .antMatchers("/review/save").hasRole("MEMBER")


                
                    .and()
                .formLogin()

                    .loginPage("/login")
                    .permitAll()
                    .usernameParameter("userName")
                    .passwordParameter("password")
                    .defaultSuccessUrl("/")
                    .failureUrl("/login?error")
                    .and()
                .exceptionHandling()
                    .accessDeniedPage("/404");
    }
}