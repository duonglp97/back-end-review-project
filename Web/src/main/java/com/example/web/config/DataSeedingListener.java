package com.example.web.config;


import com.example.database.entity.Person;
import com.example.database.entity.Role;
import com.example.database.repository.PersonRepository;
import com.example.database.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.HashSet;
@Component
public class DataSeedingListener implements ApplicationListener<ContextRefreshedEvent> {
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private PersonRepository personRepository;
    @Override
    public void onApplicationEvent(ContextRefreshedEvent arg0) {
//        if(roleRepository.findByName("ROLE_ADMIN")== null) {
//            roleRepository.save(new Role("ROLE_ADMIN"));
//        }
//
//        if(roleRepository.findByName("ROLE_MEMBER")== null) {
//            roleRepository.save(new Role("ROLE_MEMBER"));
//        }
//
//        if(personRepository.findByUserName("duong")== null) {
//            Person admin = new Person();
//            admin.setName("duong");
//            admin.setPassword(passwordEncoder.encode("123456"));
//            admin.setUserName("duong");
//
//            HashSet<Role> roles = new HashSet<Role>();
//            roles.add(roleRepository.findByName("ROLE_ADMIN"));
//            admin.setRoles(roles);
//            personRepository.save(admin);
//
//        }
//        if(personRepository.findByUserName("huy")== null) {
//            Person member = new Person();
//            member.setUserName("huy");
//            member.setPassword(passwordEncoder.encode("123456"));
//            member.setName("huy");
//            HashSet<Role> roles = new HashSet<Role>();
//
//            roles.add(roleRepository.findByName("ROLE_MEMBER"));
//            member.setRoles(roles);
//            personRepository.save(member);
//
//        }
    }
}
