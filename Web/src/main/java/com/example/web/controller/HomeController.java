package com.example.web.controller;

import com.example.database.constant.PersonConstant;
import com.example.database.constant.ReviewConstant;
import com.example.database.entity.*;
import com.example.database.repository.RoleRepository;
import com.example.database.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.security.Principal;
import java.util.*;

@Controller
public class HomeController {
//
//    @Autowired
//    private IProductService productService;

    @Autowired
    private ICenterService iCenterService;
    @Qualifier("personServiceImpl")
    @Autowired
    private IPersonService iPersonService;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private ISchoolService schoolService;
    @Autowired
    private IImageService imageService;
    @Autowired
    private IReviewService iReviewService;
    @Autowired
    private IContactService iContactService;

//    @RequestMapping(value = {"/", "/index"}, method = RequestMethod.GET)
//    public String index(Model model) {
//
//        model.addAttribute("name", "Home index");
//        model.addAttribute("total_product", "Total product we have: " + productService.getTotalProduct());
//
//        return "index";
//    }

    @RequestMapping(value = {"/index", "/"}, method = RequestMethod.GET)
    public String index(Model model){
        Iterable<Center> centers = iCenterService.findAll();
        System.out.println(centers);
        model.addAttribute("total_center", centers);
//        List<School> schoolList = schoolService.findAll();
//        model.addAttribute("total_School",schoolList);
        long schoolNumber = schoolService.count();
        long reviewNumber = iReviewService.count();
        model.addAttribute("schoolNumber",schoolNumber);
        model.addAttribute("reviewNumber",reviewNumber);

        List<School> listSchoolCauGiay = schoolService.findAllByAddressContaining("Cau Giay");
        List<School> listSchoolHaiBaTrung = schoolService.findAllByAddressContaining("Hai Ba Trung");
        List<School> listSchoolThanhXuan = schoolService.findAllByAddressContaining("Thanh Xuân");
        List<School> listSchoolTuLiem = schoolService.findAllByAddressContaining("Tu Liem");
        List<School> listSchoolHaDong = schoolService.findAllByAddressContaining("Ha Dong");

        model.addAttribute("listSchoolHaDong",listSchoolHaDong);
        model.addAttribute("listSchoolCauGiay",listSchoolCauGiay);
        model.addAttribute("listSchoolHaiBaTrung",listSchoolHaiBaTrung);
        model.addAttribute("listSchoolThanhXuan",listSchoolThanhXuan);
        model.addAttribute("listSchoolTuLiem",listSchoolTuLiem);




        return "index";
    }

    @GetMapping(value = "/school/search")
    public String search(@RequestParam("search") String search, Model model, RedirectAttributes redirectAttributes){
        List<School> searchSchoolList = schoolService.findAllByNameContaining(search);
        model.addAttribute("count",searchSchoolList.size());
        model.addAttribute("searchSchoolList",searchSchoolList);
        return "searchResult";
    }

    @GetMapping(value = "/login")
    public String login(){
        return "login";
    }

    @GetMapping(value = "/404")
    public String accessDenied(){
        return "404";
    }

    @GetMapping(value = "/register")
    public String register(Model model){
        Person newPerson = new Person();
        model.addAttribute("person",newPerson);


        return "register";
    }

    @PostMapping(value = "/person/save")
    public String save(@Valid Person addPerson, BindingResult result, RedirectAttributes redirect){
        if (result.hasErrors()) {
            return "register";
        }

        if (iPersonService.findByUserName(addPerson.getUserName())==null){
            Person newPerson = new Person();
            newPerson.setName(addPerson.getName());
            newPerson.setAddress(addPerson.getAddress());
            newPerson.setAddress(addPerson.getAddress());
            newPerson.setUserName(addPerson.getUserName());
            newPerson.setDob(addPerson.getDob());
            newPerson.setReviewSet(null);
            newPerson.setStatus(PersonConstant.ACTIVE_STATUS);
            newPerson.setPassword(passwordEncoder.encode(addPerson.getPassword()));


            HashSet<Role> roles = new HashSet<>();
            roles.add(roleRepository.findByName("ROLE_MEMBER"));
            newPerson.setRoles(roles);
            iPersonService.save(newPerson);
            return "redirect:/index";
        }else{
            return "redirect:/register";
        }
    }

//    @GetMapping(value = "/center")
//    public String centerDetail(){
//        return "centerDetail";
//    }
    @GetMapping(value = "/school/{id}")
    public String getDetailSchool(@PathVariable("id") int id, Model model){
        School school = schoolService.findById(id);
        model.addAttribute("school",school);
        long countReview = iReviewService.countBySchool(school);
        model.addAttribute("countReview",countReview);
        List<Review> reviewList = iReviewService.findAllBySchoolAndPublish(school,ReviewConstant.Publish);
        model.addAttribute("reviewList", reviewList);
        Review review = new Review();
        model.addAttribute("review", review);
        return "schoolDetail";
    }
    @PostMapping(value = "/review/save")
    public String writeReview(@ModelAttribute("review") Review review,@RequestParam("schoolId") int schoolId, Principal principal, RedirectAttributes redirect){
        Review saveReview = new Review();
        Person person = iPersonService.findByUserName(principal.getName());
        School school = schoolService.findById(schoolId);
        saveReview.setPerson(person);
        saveReview.setSchool(school);
        saveReview.setTitle(review.getTitle());
        saveReview.setAdvantage(review.getAdvantage());
        saveReview.setWeakness(review.getWeakness());
        saveReview.setEvaluation(review.getEvaluation());
        saveReview.setPublish(ReviewConstant.UnPublish);
        saveReview.setAnonymous(review.getCheckBox() == true ? ReviewConstant.Anonymous : ReviewConstant.UnAnonymous);
        iReviewService.save(saveReview);
        redirect.addFlashAttribute("success","Bạn đã viết bài review thành công!");
        return "redirect:/index";
    }

    @GetMapping(value = "/center/{id}")
    public String getCenterDetail(@PathVariable("id") int id, Model model){
        Center center = iCenterService.findById(id);
        model.addAttribute("center",center);
        return "centerDetail";
    }

    @GetMapping(value = "/contact")
    public String contact(Model model){
        Contact contact = new Contact();
        contact.setTitle("Liên hệ làm đối tác với website");
        model.addAttribute("contact", contact);
        return "contact";
    }

    @PostMapping(value = "/contact/save")
    public String saveContact(@ModelAttribute("contact") Contact contact,RedirectAttributes redirect){

        iContactService.save(contact);
        redirect.addFlashAttribute("success","Bạn đã liên lạc thành công với chúng tôi!");
        return "redirect:/index";


    }
}