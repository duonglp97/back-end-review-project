package com.example.database.service;

import com.example.database.entity.Review;
import com.example.database.entity.School;

import java.util.List;

public interface IReviewService {
    List<Review> findBySchool(School school);
    void save(Review review);
    void delete(int id);
    List<Review> findAll();
    Review findOne(int id);
    void deleteAllBySchool(School school);
    long countBySchool(School school);
    long count();
    List<Review> findAllBySchoolAndPublish(School school,int publish);
}
