package com.example.database.service.impl;


import com.example.database.entity.School;
import com.example.database.repository.SchoolRepository;
import com.example.database.service.ISchoolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SchoolServiceImpl implements ISchoolService {
    @Autowired
    private SchoolRepository schoolRepository;

    @Override
    public List<School> findAll() {
        return schoolRepository.findAll();
    }

    @Override
    public School findById(int id) {
        return schoolRepository.findById(id);
    }

    @Override
    public void deleteById(int id) {
        schoolRepository.deleteById(id);
    }

    @Override
    public void save(School school) {
        schoolRepository.save(school);
    }

    @Override
    public List<School> findAllByAddressContaining(String name) {
        return schoolRepository.findAllByAddressContaining(name);
    }

    @Override
    public long count() {
        return schoolRepository.count();
    }

    @Override
    public List<School> findAllByNameContaining(String name) {
        return schoolRepository.findAllByNameContaining(name);
    }
}
