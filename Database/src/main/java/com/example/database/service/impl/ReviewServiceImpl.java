package com.example.database.service.impl;


import com.example.database.entity.Review;
import com.example.database.entity.School;
import com.example.database.repository.ReviewRepository;
import com.example.database.service.IReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReviewServiceImpl implements IReviewService {
    @Autowired
    private ReviewRepository reviewRepository;
    @Override
    public List<Review> findBySchool(School school) {
        return reviewRepository.findBySchool(school);
    }

    @Override
    public void save(Review review) {
        reviewRepository.save(review);
    }

    @Override
    public void delete(int id) {
        reviewRepository.deleteById(id);
    }

    @Override
    public List<Review> findAll() {
        return reviewRepository.findAll();
    }

    @Override
    public Review findOne(int id) {
        return reviewRepository.findById(id);
    }

    @Override
    public void deleteAllBySchool(School school) {
        reviewRepository.deleteAllBySchool(school);
    }

    @Override
    public long countBySchool(School school) {
        return reviewRepository.countBySchool(school);
    }

    @Override
    public long count() {
        return reviewRepository.count();
    }

    @Override
    public List<Review> findAllBySchoolAndPublish(School school, int publish) {
        return reviewRepository.findAllBySchoolAndPublish(school,publish);
    }


}
