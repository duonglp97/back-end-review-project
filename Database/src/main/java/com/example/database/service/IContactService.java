package com.example.database.service;

import com.example.database.entity.Contact;

import java.util.List;

public interface IContactService {
    void save(Contact contact);
    List<Contact> findAll();
    void deleteById(int id);

}
