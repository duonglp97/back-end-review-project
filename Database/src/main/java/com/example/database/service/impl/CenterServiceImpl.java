package com.example.database.service.impl;


import com.example.database.entity.Center;
import com.example.database.repository.CenterRepository;
import com.example.database.service.ICenterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CenterServiceImpl implements ICenterService {
    @Autowired
    private CenterRepository centerRepository;


    @Override
    public List<Center> findAll() {
        return centerRepository.findAll();
    }

    @Override
    public Optional<Center> findByName(String name) {
        return centerRepository.findByNameContaining(name);
    }

    @Override
    public void save(Center center) {
        centerRepository.save(center);

    }

    @Override
    public void delete(int id) {
        centerRepository.deleteById(id);
    }

    @Override
    public long count() {
        return centerRepository.count();
    }

    @Override
    public Center findById(int id) {
        return centerRepository.findById(id);
    }
}
