package com.example.database.service;


import com.example.database.entity.Center;

import java.util.List;
import java.util.Optional;

public interface ICenterService {
    List<Center> findAll();
    Optional<Center> findByName(String name);
    void save(Center center);
    void delete(int id);
    long count();
    Center findById(int id);

}
