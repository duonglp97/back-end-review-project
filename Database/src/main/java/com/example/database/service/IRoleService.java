package com.example.database.service;

import com.example.database.entity.Role;

public interface IRoleService {
    Role findByName(String name);
}
