package com.example.database.service;

import com.example.database.entity.School;

import java.util.List;

public interface ISchoolService {
    List<School> findAll();
    School findById(int id);
    void deleteById(int id);
    void save(School school);
    List<School> findAllByAddressContaining(String name);
    long count();
    List<School> findAllByNameContaining(String name);

}
