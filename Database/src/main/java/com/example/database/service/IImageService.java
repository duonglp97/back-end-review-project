package com.example.database.service;

import com.example.database.entity.Center;
import com.example.database.entity.Image;
import com.example.database.entity.School;

import java.util.List;

public interface IImageService {
    void delete(int id);
    void save(Image image);
    List<Image> findImagesBySchool(int id);
    void deleteAllByCenter(Center center);
    void deleteAllBySchool(School school);
    Image findByUrl(String url);

}
