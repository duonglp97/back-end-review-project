package com.example.database.service.impl;

import com.example.database.entity.Contact;
import com.example.database.repository.ContactRepository;
import com.example.database.service.IContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContactServiceImpl implements IContactService {
    @Autowired
    private ContactRepository contactRepository;
    @Override
    public void save(Contact contact) {
        contactRepository.save(contact);
    }

    @Override
    public List<Contact> findAll() {
        return contactRepository.findAll();
    }

    @Override
    public void deleteById(int id) {
        contactRepository.deleteById(id);
    }
}
