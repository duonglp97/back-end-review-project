package com.example.database.service;

import com.example.database.entity.Person;
import com.example.database.entity.Role;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface IPersonService extends UserDetailsService {
    List<Person> findAll();
    Person findByUserName(String name);
    void save(Person person);
    List<Person> findAllByRoles(Role role);
    Person findById(int id);
    Person findByUserNameAndStatusAndRoles(String name,int status,Role role);

}
