package com.example.database.service.impl;


import com.example.database.constant.PersonConstant;
import com.example.database.entity.Person;
import com.example.database.entity.Role;
import com.example.database.repository.PersonRepository;
import com.example.database.repository.RoleRepository;
import com.example.database.service.IPersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class PersonServiceImpl implements IPersonService {

    @Autowired
    private PersonRepository personRepository;
    @Autowired
    private RoleRepository roleRepository;



    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Person person = personRepository.findByUserNameAndStatusAndRoles(username, PersonConstant.ACTIVE_STATUS, roleRepository.findByName("ROLE_MEMBER"));

        if(person == null){
            throw new UsernameNotFoundException("user not found");
        }
        Set<GrantedAuthority> grantedAuthorities = new HashSet<GrantedAuthority>();
        Set<Role> roles = person.getRoles();

        for (Role role: roles
             ) {
            grantedAuthorities.add(new SimpleGrantedAuthority(role.getName()));

        }
        return new org.springframework.security.core.userdetails.User (person.getName(),person.getPassword(),grantedAuthorities);
    }

    @Override
    public List<Person> findAll() {
        return personRepository.findAll();
    }

    @Override
    public Person findByUserName(String name) {
        return personRepository.findByUserName(name);
    }

    @Override
    public void save(Person person) {
        personRepository.save(person);
    }

    @Override
    public List<Person> findAllByRoles(Role role) {
        return personRepository.findAllByRoles(role);
    }

    @Override
    public Person findById(int id) {
        return personRepository.findById(id);
    }

    @Override
    public Person findByUserNameAndStatusAndRoles(String name, int status, Role role) {
        return personRepository.findByUserNameAndStatusAndRoles(name,status,role);
    }


}
