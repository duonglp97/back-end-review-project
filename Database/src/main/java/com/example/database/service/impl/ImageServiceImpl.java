package com.example.database.service.impl;


import com.example.database.entity.Center;
import com.example.database.entity.Image;
import com.example.database.entity.School;
import com.example.database.repository.ImageRepository;
import com.example.database.service.IImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ImageServiceImpl implements IImageService {
    @Autowired
    private ImageRepository imageRepository;

    @Override
    public void delete(int id) {
        imageRepository.deleteById(id);
    }

    @Override
    public void save(Image image) {
        imageRepository.save(image);
    }

    @Override
    public List<Image> findImagesBySchool(int id) {
        return imageRepository.findImagesBySchoolId(id);
    }

    @Override
    public void deleteAllByCenter(Center center) {
        imageRepository.deleteAllByCenter(center);
    }

    @Override
    public void deleteAllBySchool(School school) {
        imageRepository.deleteAllBySchool(school);
    }

    @Override
    public Image findByUrl(String url) {
        return imageRepository.findByUrl(url);
    }


}
