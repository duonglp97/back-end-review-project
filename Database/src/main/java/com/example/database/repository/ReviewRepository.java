package com.example.database.repository;


import com.example.database.entity.Review;
import com.example.database.entity.School;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReviewRepository extends CrudRepository<Review,Integer> {
    List<Review> findBySchool(School school);
    Review findById(int id);
    List<Review> findAll();
    void deleteAllBySchool(School school);
    long countBySchool(School school);
    List<Review> findAllBySchoolAndPublish(School school, int publish);
}
