package com.example.database.repository;


import com.example.database.entity.Center;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CenterRepository extends CrudRepository<Center, Integer> {
    Optional<Center> findByNameContaining(String name);
    Center findById(int id);
    List<Center> findAll();
}
