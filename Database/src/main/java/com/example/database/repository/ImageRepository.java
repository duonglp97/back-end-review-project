package com.example.database.repository;


import com.example.database.entity.Center;
import com.example.database.entity.Image;
import com.example.database.entity.School;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ImageRepository extends CrudRepository<Image,Integer> {

    List<Image> findImagesBySchoolId(int id);

    void deleteAllByCenter(Center center);

    void deleteAllBySchool(School school);

    Image findByUrl(String url);



}
