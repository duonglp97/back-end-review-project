package com.example.database.repository;


import com.example.database.entity.School;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SchoolRepository extends CrudRepository<School,Integer> {
    List<School> findAll();
    School findById(int id);
    List<School> findAllByAddressContaining(String address);
    List<School> findAllByNameContaining(String name);


}
