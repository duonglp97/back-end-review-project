package com.example.database.entity;


import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "review")
public class Review {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "review_id")
    private int id;
    @Column(name = "title")
    private String title;
    @Column(name = "evaluation")
    private int evaluation;
    @Column(name = "advantage")
    private String advantage;
    @Column(name = "weakness")
    private String weakness;
    @Column(name = "anonymous")
    private int anonymous;
    @Column(name = "publish")
    private int publish;
    @ManyToOne
    @JoinColumn(name = "person_id")
    private Person person;

    @Transient
    private boolean checkBox;
//    @Transient
//    private int schoolId;
//
//    public int getSchoolId() {
//        return schoolId;
//    }
//
//    public void setSchoolId(int schoolId) {
//        this.schoolId = schoolId;
//    }

    public boolean getCheckBox() {
        return checkBox;
    }

    public void setCheckBox(boolean checkBox) {
        this.checkBox = checkBox;
    }

    @ManyToOne
    @JoinColumn(name = "school_id")
    private School school;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(int evaluation) {
        this.evaluation = evaluation;
    }

    public String getAdvantage() {
        return advantage;
    }

    public void setAdvantage(String advantage) {
        this.advantage = advantage;
    }

    public String getWeakness() {
        return weakness;
    }

    public void setWeakness(String weakness) {
        this.weakness = weakness;
    }

    public int getAnonymous() {
        return anonymous;
    }

    public void setAnonymous(int anonymous) {
        this.anonymous = anonymous;
    }

    public int getPublish() {
        return publish;
    }

    public void setPublish(int publish) {
        this.publish = publish;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }



}
