package com.example.database.entity;


import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.Set;

@Entity
@Table(name = "school")
public class School {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "school_id")
    private int id;
    @Column(name = "name")
    @NotEmpty
    private String name;
    @Column(name = "address")
    @NotEmpty
    private String address;

    @Column(name = "history")
    @NotEmpty
    private String history;
    @Column(name = "major")
    private String major;

    @Transient
    private String lstFile;



    @OneToMany(mappedBy = "school")
    private Set<Review> reviewSet;
    @OneToMany(mappedBy = "school")
    private Set<Image> imageSet;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getHistory() {
        return history;
    }

    public void setHistory(String history) {
        this.history = history;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public Set<Review> getReviewSet() {
        return reviewSet;
    }

    public void setReviewSet(Set<Review> reviewSet) {
        this.reviewSet = reviewSet;
    }

    public Set<Image> getImageSet() {
        return imageSet;
    }

    public void setImageSet(Set<Image> imageSet) {
        this.imageSet = imageSet;
    }
    public String getLstFile() {
        return lstFile;
    }

    public void setLstFile(String lstFile) {
        this.lstFile = lstFile;
    }
}
