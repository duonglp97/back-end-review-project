package com.example.database.constant;

public class ReviewConstant {
    public static final int Publish = 1;
    public static final int UnPublish = 0;
    public static final int Anonymous = 1;
    public static final int UnAnonymous = 0;
}
