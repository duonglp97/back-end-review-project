package com.example.database.constant;

public class PersonConstant {
    public static final int ACTIVE_STATUS = 0;
    public static final int NONACTIVE_STATUS = 1;
}
